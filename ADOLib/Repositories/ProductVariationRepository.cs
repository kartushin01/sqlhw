﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ADOLib.Repositories
{
    public class ProductVariationRepository : IProductVariationReository
    {
     
        private string TableName = "Variations";
       
        public List<ProductVarioation> AllToProduct(int productId)
        {
            List<ProductVarioation> variationList =
                new List<ProductVarioation>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    $"Select * From {TableName} WHERE productId = {productId} ";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ProductVarioation variation = new ProductVarioation();
                        variation.Id = Convert.ToInt32(dataReader["Id"]);
                        variation.Price = (decimal) dataReader["Price"];
                        variation.Article = dataReader["Article"].ToString();
                        variation.Description = (string) dataReader["Description"];
                        variationList.Add(variation);
                    }
                }
                connection.Close();
            }

            return variationList;
        }

        public ProductVarioation GetById(int id)
        {
           ProductVarioation variation = new ProductVarioation();

           using (SqlConnection connection =
               new SqlConnection(Settings.ConnectionString))
           {
               connection.Open();

               string sql = $"Select * From {TableName} WHERE Id = {id}";

               SqlCommand command = new SqlCommand(sql, connection);

               using (SqlDataReader dataReader = command.ExecuteReader())
               {
                   while (dataReader.Read())
                   {
                       variation.Id = Convert.ToInt32(dataReader["Id"]);
                       variation.Article = (string)dataReader["Article"];
                       variation.Description = (string) dataReader["Description"];
                       variation.ProductId = (int) dataReader["ProductId"];
                       variation.Price = (decimal) dataReader["Price"];
                   }
               }

               connection.Close();
           }

            return variation;
        }

        public void Create(ProductVarioation variation) 
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Insert Into {TableName} (Description, Article, Price, ProductId ) Values (N'{variation.Description}', N'{variation.Article}', '{variation.Price}', {variation.ProductId})";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        
        public void Edit(ProductVarioation variation)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET Description = N'{variation.Description}', Article = '{variation.Article}', Price = '{variation.Price}', ProductId = '{variation.ProductId}' Where Id='{variation.Id}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        public void Delete(ProductVarioation variation)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sqlDelete =
                    $"Delete From {TableName} Where Id='{variation.Id}'";
                using (SqlCommand command =
                    new SqlCommand(sqlDelete, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        public bool ProductVarioationExists(int id)
        {
            int exist;
            string sql = $"SELECT COUNT(*) From {TableName} Where Id='{id}'";
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    exist = (int)command.ExecuteScalar();
                    connection.Close();
                }
            }

            if (exist > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
