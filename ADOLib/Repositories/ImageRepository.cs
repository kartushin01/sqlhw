﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ADOLib.Repositories
{
    public class ImageRepository : IImageRepository
    {
        private string TableName = "Images";
        public List<ProductImage> GetAlltoProduct(int productId)
        {
            List<ProductImage> imagesList = new List<ProductImage>();
            
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();
                
                string sql = $"Select * From {TableName}  WHERE productId = {productId}";

                SqlCommand command = new SqlCommand(sql, connection);
                
                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ProductImage image = new ProductImage();
                        image.Id = Convert.ToInt32(dataReader["Id"]);
                        image.Url = (string)dataReader["Url"];
                        imagesList.Add(image);
                    }
                }
                connection.Close();
            }

          return imagesList;
        }

        public ProductImage GetById(int id)
        {
            ProductImage comment = new ProductImage();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql = $"Select * From {TableName} WHERE Id = {id}";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        comment.Id = Convert.ToInt32(dataReader["Id"]);
                        comment.Url = (string)dataReader["Url"];
                    }
                }

                connection.Close();
            }

            return comment;
        }

        public void Create(ProductImage img)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Insert Into {TableName} (Url, ProductId ) Values (N'{img.Url}', '{img.ProductId}')";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Edit(ProductImage img)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET Url = N'{img.Url}', Rating = '{img.ProductId}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sqlDelete =
                    $"Delete From {TableName} Where Id='{id}'";
                using (SqlCommand command =
                    new SqlCommand(sqlDelete, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public bool ProductImageExists(int id)
        {
            int exist;
            string sql = $"SELECT COUNT(*) From {TableName} Where Id='{id}'";
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    exist = (int)command.ExecuteScalar();
                    connection.Close();
                }
            }

            if (exist > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
