﻿using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;

namespace ADOLib.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private string TableName = "Categories";
       
        public List<Category> GetAllWhitChildren()
        {
            String commandText =
                @";WITH Hierarchy AS
              (
                 SELECT
                    Id,  ParentCategoryId = CAST(NULL AS INT),
                    Name, HierLevel = 1
                 FROM
                    dbo.Categories  
                 WHERE
                    ParentCategoryId IS NULL
                 UNION ALL

                 SELECT
                    ht.Id, ht.ParentCategoryId, ht.Name, h1.HierLevel + 1
                 FROM
                    dbo.Categories ht  
                 INNER JOIN 
                    Hierarchy h1 ON ht.ParentCategoryId = h1.ID
              )
              SELECT *
              FROM Hierarchy
              ORDER BY HierLevel, Id";

            List<Category> categories = new List<Category>();

            using (SqlConnection conn = new SqlConnection(Settings.ConnectionString))
            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                conn.Open();
                // use SqlDataReader to iterate over results
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        // get the info from the reader into the "Unit" object
                        Category thisCategory = new Category();

                        thisCategory.Id = rdr.GetInt32(0);
                        thisCategory.Name = rdr.GetString(2);
                        thisCategory.Children = new List<Category>();

                        // check if we have a parent
                        if (!rdr.IsDBNull(1))
                        {
                            // get ParentId
                            int parentId = rdr.GetInt32(1);

                            // find parent in list of categories already loaded
                            Category parent =
                                categories.FirstOrDefault(u =>
                                    u.Id == parentId);

                            // if parent found - set this categories's parent to that object
                            if (parent != null)
                            {
                                thisCategory.ParentCategory = parent;
                                parent.Children.Add(thisCategory);
                            }
                        }
                        else
                        {
                            categories.Add(thisCategory);
                        }
                    }
                }

                conn.Close();
            }

            return categories;
        }

        public Category GetById(int id)
        {
            Category category = new Category();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql = $"Select * From {TableName} WHERE Id = {id}";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        category.Id = Convert.ToInt32(dataReader["Id"]);
                        category.Name = (string) dataReader["Name"];

                        category.ParentCategoryId =
                            String.IsNullOrEmpty(
                                Convert.ToString(
                                    dataReader["ParentCategoryId"]))
                                ? (int?) null
                                : int.Parse(dataReader["ParentCategoryId"]
                                    .ToString());
                    }
                }

                connection.Close();
            }

            return category;
        }

        public void Create(Category category)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Insert Into {TableName} (Name) Values (N'{category.Name}')";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Edit(Category category)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET Name = N'{category.Name}', ParentCategoryId = @ParentId Where Id='{category.Id}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@ParentId",
                        (object) category.ParentCategoryId ?? DBNull.Value);
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Delete(Category category)
        {
            //Изменить ParentCategoryId на null у всех дочерних
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET ParentCategoryId = NULL Where ParentCategoryId='{category.Id}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }

            //Удаление
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sqlDelete =
                    $"Delete From {TableName} Where Id='{category.Id}'";
                using (SqlCommand command =
                    new SqlCommand(sqlDelete, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public bool CategoryExists(int id)
        {
            int exist;
            string sql = $"SELECT COUNT(*) From {TableName} Where Id='{id}'";
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    exist = (int) command.ExecuteScalar();
                    connection.Close();
                }
            }

            if (exist > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
