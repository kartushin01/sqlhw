﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ADOLib.Repositories
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        private string TableName = "ProductCategories";
      
        public List<ProductCategory> GetProductsCategories(int productId)
        {
            List<ProductCategory> pkList = new List<ProductCategory>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    $"Select * From {TableName} WHERE productId = {productId} ";

                SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@productIdVal", productId);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ProductCategory pk = new ProductCategory();
                        pk.CategoryId = Convert.ToInt32(dataReader["CategoryId"]);
                        pk.ProductId = (int)dataReader["ProductId"];

                        pkList.Add(pk);
                    }
                }

                connection.Close();
            }

            return pkList;
        }
    }
}
