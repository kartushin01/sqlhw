﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ADOLib.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private string TableName = "Catalog";

        public List<Product> All()
        {
            List<Product> productList = new List<Product>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    $"Select * From {TableName}";

                SqlCommand command = new SqlCommand(sql, connection);
                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Product product = new Product();
                        product.Id = Convert.ToInt32(dataReader["Id"]);
                        product.Name = (string) dataReader["Name"];
                        product.Description =
                            (string) dataReader["Description"];
                        productList.Add(product);
                    }
                }

                connection.Close();
            }

            return productList;
        }

        public List<Product> GetAllinCategory(int categoryId)
        {
            List<Product> productsList =
                new List<Product>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    $"Select * From {TableName} WHERE productId = {categoryId} ";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Product product = new Product();
                        product.Id = Convert.ToInt32(dataReader["Id"]);
                        product.Name = (string) dataReader["Name"];
                        product.Description =
                            (string) dataReader["Description"];
                        productsList.Add(product);
                    }
                }
                connection.Close();
            }

            return productsList;
        }

        public List<Product> GetAllProductsFromCategoryAndChildrenCategories(int categoryId)
        {
            List<Product> productsList =
                new List<Product>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    @"SELECT * FROM Catalog
                JOIN ProductCategories AS pc ON pc.productId = Catalog.id
                JOIN Categories AS cats ON pc.CategoryId = cats.id
                WHERE cats.Id = @catgeryId OR cats.ParentCategoryId = @catgeryId ORDER BY cats.id";

                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@catgeryId", categoryId);
                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Product product = new Product();
                        product.Id = Convert.ToInt32(dataReader["Id"]);
                        product.Name = (string)dataReader["Name"];
                        product.Description =
                            (string)dataReader["Description"];
                        productsList.Add(product);
                    }
                }
                connection.Close();
            }

            return productsList;
        }

        public Product GetById(int id)
        {
            Product product = new Product();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql = $"Select * From {TableName} WHERE Id = {id}";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        product.Id = Convert.ToInt32(dataReader["Id"]);
                        product.Name = (string)dataReader["Name"];
                        product.Description = (string)dataReader["Description"];
                    }
                }

                connection.Close();
            }

            return product;
        }

        public void Create(Product product)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Insert Into {TableName} (Name, Description ) Values (N'{product.Name}', N'{product.Description}')";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Edit(Product product)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET Name = N'{product.Name}', Description = '{product.Description}' Where Id='{product.Id}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Delete(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
