﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;

namespace ADOLib.Repositories
{
    public class CommentsRepository : ICommentRepository
    {
        private string TableName = "Comments";
     
        public List<Comment> All()
        {
            using (SqlConnection connection = new SqlConnection(Settings.ConnectionString))
            {
                List<Comment> comments = new List<Comment>();

                connection.Open();

                string sql = $"Select * From {TableName}";
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Comment comment = new Comment();
                        comment.Id = Convert.ToInt32(dataReader["Id"]);
                        comment.Text = (string)dataReader["Text"];
                        comment.Rating = (double)dataReader["Rating"];
                        comment.ProductId = (int) dataReader["ProductId"];
                        comments.Add(comment);
                    }
                }

                connection.Close();

                return comments;
            }
        }

        public List<Comment> GetAllForProduct(int productId)
        {
            List<Comment> commentsList =
                new List<Comment>();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql =
                    $"Select * From {TableName} WHERE productId = @productIdVal ";

                SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@productIdVal", productId);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        Comment comment = new Comment();
                        comment.Id = Convert.ToInt32(dataReader["Id"]);
                        comment.Text = (string)dataReader["Text"];
                        comment.Author = (string)dataReader["Author"];
                        comment.Rating = (double)dataReader["Rating"];
                        comment.DatePublish = (DateTime)dataReader["DatePublish"];
                        comment.ProductId = (int)dataReader["ProductId"];
                        commentsList.Add(comment);
                    }
                }

                connection.Close();
            }

            return commentsList;
        }

        public Comment GetById(int id)
        {
            Comment comment = new Comment();

            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();

                string sql = $"Select * From {TableName} WHERE Id = {id}";

                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        comment.Id = Convert.ToInt32(dataReader["Id"]);
                        comment.Text = (string)dataReader["Text"];
                        comment.Author = (string)dataReader["Author"];
                        comment.Rating = (double)dataReader["Rating"];
                        comment.DatePublish = (DateTime) dataReader["DatePublish"];
                        comment.ProductId = (int)dataReader["ProductId"];
                    }
                }

                connection.Close();
            }

            return comment;
        }

        public void Create(Comment comment)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Insert Into {TableName} (Text, Rating, Author, DatePublish, ProductId ) Values (N'{comment.Text}', N'{comment.Rating}', '{comment.Author}', '{comment.DatePublish}', {comment.ProductId})";
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
    

        public void Edit(Comment comment)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sql =
                    $"Update {TableName} SET Text = N'{comment.Text}', Rating = '{comment.Rating}', Author = '{comment.Author}', DatePublish = '{comment.DatePublish}, ProductId = '{comment.ProductId}'  Where Id='{comment.Id}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public void Delete(int id)
        {
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                string sqlDelete =
                    $"Delete From {TableName} Where Id='{id}'";
                using (SqlCommand command =
                    new SqlCommand(sqlDelete, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public bool CommentExists(int id)
        {
            int exist;
            string sql = $"SELECT COUNT(*) From {TableName} Where Id='{id}'";
            using (SqlConnection connection =
                new SqlConnection(Settings.ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    exist = (int)command.ExecuteScalar();
                    connection.Close();
                }
            }

            if (exist > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
