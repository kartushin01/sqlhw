﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using Microsoft.EntityFrameworkCore;

namespace EfLib.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(CatalogContext dbContext) : base(dbContext)
        {
        }
        public List<Comment> GetAllForProduct(int productId)
        {
            return _dbContext.Comments.Include(p => p.Product).Where(p => p.ProductId == productId).ToList();
        }
    }
}
