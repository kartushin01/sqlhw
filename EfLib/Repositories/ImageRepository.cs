﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using Microsoft.EntityFrameworkCore;

namespace EfLib.Repositories
{
    public class ImageRepository : Repository<ProductImage>, IImageRepository
    {
       
        public ImageRepository(CatalogContext dbContext): base(dbContext)
        {
            
        }

        public List<ProductImage> GetAlltoProduct(int productId)
        {
            return _dbContext.Images.Where(p => p.ProductId == productId).ToList();
        }

    }
}
