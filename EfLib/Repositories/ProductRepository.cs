﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using Microsoft.EntityFrameworkCore;

namespace EfLib.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(CatalogContext dbContext) : base(dbContext)
        {
        }

        public List<Product> GetAllinCategory(int categoryId)
        {
            var category =
                _dbContext.Categories.FirstOrDefault(c => c.Id == categoryId);

            return _dbContext.CatalogItems.Where(c =>
                    c.ProductCategories.Any(i => i.CategoryId == categoryId))
                .ToList();
        }

        public List<Product> GetAllProductsFromCategoryAndChildrenCategories(int categoryId)
        {
            List<Product> products = new List<Product>();

            var category = _dbContext.Categories.Include(c => c.Children)
                .FirstOrDefault(c => c.Id == categoryId);

            products.AddRange(
                GetAllinCategory(categoryId)); //Все товары их этой категории

            foreach (var chilCategory in category.Children) //плюс товары из дочерних категорий
            {
                products.AddRange(_dbContext.CatalogItems.Where(c =>
                    c.ProductCategories.Any(p =>
                        p.CategoryId == chilCategory.Id)).ToList());
            }

            return products;
        }
    }
}
