﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;

namespace EfLib.Repositories
{
    public class ProductVariationRepository  : Repository<ProductVarioation>, IProductVariationReository
    {
      
        public ProductVariationRepository(CatalogContext dbContext):base(dbContext)
        {
            
        }

        public List<ProductVarioation> AllToProduct(int id)
        {
            return _dbContext.Variations.Where(p=>p.ProductId == id).ToList();
        }

   
       
    }
}
