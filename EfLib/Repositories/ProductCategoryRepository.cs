﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;

namespace EfLib.Repositories
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        protected readonly CatalogContext _dbContext;

        public ProductCategoryRepository(CatalogContext dbContext)
        {
            _dbContext = dbContext;
        }


        public List<ProductCategory> GetProductsCategories(int productId)
        {
            return _dbContext.ProductCategories.Where(i => i.ProductId == productId).ToList();
        }
    }
}
