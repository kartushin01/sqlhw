﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using Microsoft.EntityFrameworkCore;

namespace EfLib.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
       
        public CategoryRepository(CatalogContext dbContext) : base(dbContext)
        {
        }

        public List<Category> GetAllWhitChildren()
        {
            return _dbContext.Categories.Where(p => p.ParentCategoryId == null)
                .Include(c => c.Children).ToList();
        }

      

    }
}
