﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories");
            builder.HasKey(c => c.Id);
            //builder.Property(c => c.Id)
            //    .UseHiLo("category_hilo")
            //    .IsRequired();

            builder.Property(cb => cb.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.HasMany(c => c.Children)
                .WithOne(c => c.ParentCategory)
                .HasForeignKey(c => c.ParentCategoryId);

            builder.HasData(
                new Category() { Name = "Книги", Id = 1 },
                new Category() { Name = "Программирование", Id = 2, ParentCategoryId = 1 },
                new Category() { Name = "Кулинария", Id = 3, ParentCategoryId = 1 },
                new Category() { Name = "Электроника", Id = 4 },
                new Category() { Name = "Смартфоны", Id = 5, ParentCategoryId = 4 },
                new Category() { Name = "Телевизоры", Id = 6, ParentCategoryId = 4 },
                new Category() { Name = "Хит продаж", Id = 7 });
        }
    }
}
