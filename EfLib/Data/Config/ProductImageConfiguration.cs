﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class ProductImageConfiguration : IEntityTypeConfiguration<ProductImage>
    {
        public void Configure(EntityTypeBuilder<ProductImage> builder)
        {
            builder.ToTable("Images");
            builder.HasKey(c => c.Id);
            builder.HasOne(p => p.Product)
                .WithMany(c => c.Images)
                .HasForeignKey(p => p.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(
                new ProductImage()
                {
                    Id = 1,
                    ProductId = 1,
                    Url = "https://static.my-shop.ru/product/3/165/1642017.jpg"
                },
                new ProductImage()
                {
                    Id = 2,
                    ProductId = 2,
                    Url = "https://cdn1.ozone.ru/multimedia/c1200/1004849293.jpg"
                },
                new ProductImage()
                {
                    Id = 3,
                    ProductId = 3,
                    Url =
                        "https://static.re-store.ru/upload/resize_cache/iblock/08c/1120_1496_17f5c944b3b71591cc9304fac25365de2/08cdbcf697c6aa451b3fcde646bb4310.jpg"
                },
                new ProductImage()
                {
                    Id = 4,
                    ProductId = 4,
                    Url = "https://www.lg.com/ru/images/televisions/md06186036/gallery/1100_75UM7090_01.jpg"
                }
            );
        }
    }
}
