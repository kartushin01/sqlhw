﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class ProductCommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable("Comments");

            builder.HasKey(c => c.Id);

            //builder.Property(c => c.Id)
            //    .UseHiLo("comment_hilo")
            //    .IsRequired();

            builder.Property(c => c.Text)
                .IsRequired(true)
                .HasMaxLength(500);

            builder.Property(r => r.Rating).IsRequired();

            builder.HasOne(p => p.Product)
                .WithMany(c => c.Comments)
                .HasForeignKey(i => i.ProductId)
                .OnDelete(DeleteBehavior.Cascade);


            builder.HasData(
                new Comment()
                {
                    Id = 1,
                    ProductId = 1,
                    Author = "User1",
                    DatePublish = DateTime.Now,
                    Rating = 5,
                    Text = "Text comment 1"
                },
                new Comment()
                {
                    Id = 2,
                    ProductId = 2,
                    Author = "User2",
                    DatePublish = DateTime.Now,
                    Rating = 5,
                    Text = "Text comment 3"
                },
                new Comment()
                {
                    Id = 3,
                    ProductId = 3,
                    Author = "User3",
                    DatePublish = DateTime.Now,
                    Rating = 4.5,
                    Text = "Text comment 3"
                }
                );

        }
    }
}
