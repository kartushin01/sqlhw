﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class ProductVariationsConfiguration : IEntityTypeConfiguration<ProductVarioation>
    {
        public void Configure(EntityTypeBuilder<ProductVarioation> builder)
        {
            builder.ToTable("Variations");
            builder.HasKey(c => c.Id);
            builder.HasOne(p => p.Product)
                .WithMany(v => v.Variations)
                .HasForeignKey(p=>p.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(
                new { Id = 1, ProductId = 1, Price = 500m, Description = "Электрнная версия" },
                new { Id = 2, ProductId = 1, Price = 1800m, Description = "Твердый переплёт" },
                new { Id = 3, ProductId = 2, Price = 500m, Description = "Электрнная версия" },
                new { Id = 4, ProductId = 2, Price = 1100m, Description = "Твердый переплёт" },
                new { Id = 5, ProductId = 3, Price = 105000m, Description = "128gb" },
                new { Id = 6, ProductId = 3, Price = 80000m, Description = "64gb" },
                new { Id = 7, ProductId = 4, Price = 65000m, Description = "FHD" },
                new { Id = 8, ProductId = 4, Price = 85000m, Description = "UHD 4K" }
            );
        }
    }
}
