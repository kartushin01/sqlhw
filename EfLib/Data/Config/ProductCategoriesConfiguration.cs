﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class ProductCategoriesConfiguration : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.HasKey(pc => new {pc.ProductId, pc.CategoryId});

            builder.HasOne(pc => pc.Product)
                .WithMany(pc => pc.ProductCategories)
                .HasForeignKey(pc => pc.ProductId);

            builder.HasOne(pc => pc.Category)
                .WithMany(c => c.ProductCategories)
                .HasForeignKey(c => c.CategoryId);

            builder.HasData(
                new ProductCategory() { ProductId = 1, CategoryId = 2 },
                new ProductCategory() { ProductId = 2, CategoryId = 3 },
                new ProductCategory() { ProductId = 1, CategoryId = 7 },
                new ProductCategory() { ProductId = 2, CategoryId = 7 },
                new ProductCategory() { ProductId = 3, CategoryId = 5 },
                new ProductCategory() { ProductId = 3, CategoryId = 7 },
                new ProductCategory() { ProductId = 4, CategoryId = 6 }
            );
        }
    }
}
