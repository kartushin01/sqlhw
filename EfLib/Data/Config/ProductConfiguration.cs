﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfLib.Data.Config
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Catalog");
            builder.HasKey(c => c.Id);
            //builder.Property(c => c.Id)
            //    .UseHiLo("catalog_hilo")
            //    .IsRequired();

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(d => d.Description)
                .HasMaxLength(800)
                .IsRequired();

            builder.HasData(
                new Product()
                {
                    Id = 1,
                    Name = "Совершенный код. Практическое руководство",
                    Description =
                        "Более 10 лет первое издание этой книги считалось одним из лучших ..."
                },
                new Product()
                {
                    Id = 2,
                    Name = "Блюда народов мира",
                    Description = "О кухне людей в разных частях света"
                },
                new Product()
                {
                    Id = 3,
                    Name = "IPhone 11Pro",
                    Description = "---"
                },
                new Product()
                {
                    Id = 4,
                    Name = "LG55LJU",
                    Description = "LED TV"
                }
            );
        }
    }
}
