﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFLib.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Catalog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 800, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(maxLength: 500, nullable: false),
                    Rating = table.Column<double>(nullable: false),
                    Author = table.Column<string>(nullable: true),
                    DatePublish = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Catalog_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Catalog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Url = table.Column<string>(nullable: true),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Images_Catalog_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Catalog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Variations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Article = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Variations_Catalog_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Catalog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => new { x.ProductId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_ProductCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductCategories_Catalog_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Catalog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Catalog",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Более 10 лет первое издание этой книги считалось одним из лучших ...", "Совершенный код. Практическое руководство" },
                    { 2, "О кухне людей в разных частях света", "Блюда народов мира" },
                    { 3, "---", "IPhone 11Pro" },
                    { 4, "LED TV", "LG55LJU" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 1, "Книги", null },
                    { 4, "Электроника", null },
                    { 7, "Хит продаж", null }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "ParentCategoryId" },
                values: new object[,]
                {
                    { 2, "Программирование", 1 },
                    { 3, "Кулинария", 1 },
                    { 5, "Смартфоны", 4 },
                    { 6, "Телевизоры", 4 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Author", "DatePublish", "ProductId", "Rating", "Text" },
                values: new object[,]
                {
                    { 1, "User1", new DateTime(2020, 1, 29, 21, 50, 19, 79, DateTimeKind.Local).AddTicks(8564), 1, 5.0, "Text comment 1" },
                    { 2, "User2", new DateTime(2020, 1, 29, 21, 50, 19, 81, DateTimeKind.Local).AddTicks(4395), 2, 5.0, "Text comment 3" },
                    { 3, "User3", new DateTime(2020, 1, 29, 21, 50, 19, 81, DateTimeKind.Local).AddTicks(4461), 3, 4.5, "Text comment 3" }
                });

            migrationBuilder.InsertData(
                table: "Images",
                columns: new[] { "Id", "ProductId", "Url" },
                values: new object[,]
                {
                    { 4, 4, "https://www.lg.com/ru/images/televisions/md06186036/gallery/1100_75UM7090_01.jpg" },
                    { 1, 1, "https://static.my-shop.ru/product/3/165/1642017.jpg" },
                    { 3, 3, "https://static.re-store.ru/upload/resize_cache/iblock/08c/1120_1496_17f5c944b3b71591cc9304fac25365de2/08cdbcf697c6aa451b3fcde646bb4310.jpg" },
                    { 2, 2, "https://cdn1.ozone.ru/multimedia/c1200/1004849293.jpg" }
                });

            migrationBuilder.InsertData(
                table: "ProductCategories",
                columns: new[] { "ProductId", "CategoryId" },
                values: new object[,]
                {
                    { 3, 7 },
                    { 2, 7 },
                    { 1, 7 }
                });

            migrationBuilder.InsertData(
                table: "Variations",
                columns: new[] { "Id", "Article", "Description", "Price", "ProductId" },
                values: new object[,]
                {
                    { 4, null, "Твердый переплёт", 1100m, 2 },
                    { 7, null, "FHD", 65000m, 4 },
                    { 2, null, "Твердый переплёт", 1800m, 1 },
                    { 1, null, "Электрнная версия", 500m, 1 },
                    { 5, null, "128gb", 105000m, 3 },
                    { 6, null, "64gb", 80000m, 3 },
                    { 3, null, "Электрнная версия", 500m, 2 },
                    { 8, null, "UHD 4K", 85000m, 4 }
                });

            migrationBuilder.InsertData(
                table: "ProductCategories",
                columns: new[] { "ProductId", "CategoryId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 2, 3 },
                    { 3, 5 },
                    { 4, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryId",
                table: "Categories",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ProductId",
                table: "Comments",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_ProductId",
                table: "Images",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategories_CategoryId",
                table: "ProductCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Variations_ProductId",
                table: "Variations",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Images");

            migrationBuilder.DropTable(
                name: "ProductCategories");

            migrationBuilder.DropTable(
                name: "Variations");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Catalog");
        }
    }
}
