﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<ProductCategory> ProductCategories { get; set; }
        public ICollection<ProductVarioation> Variations { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<ProductImage> Images { get; set; }
        
    }
}
