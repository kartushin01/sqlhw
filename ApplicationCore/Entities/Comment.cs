﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class Comment : BaseEntity
    {
        public string Text { get; set; }
        public double Rating { get; set; }
        public string Author { get; set; }
        public DateTime DatePublish { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
