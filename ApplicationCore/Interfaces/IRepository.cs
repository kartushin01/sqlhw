﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IRepository<T> where T: BaseEntity
    {
        T GetById(int id);
        IReadOnlyList<T> GetAll();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        bool IsOnlyOneEntity(int id);
    } 
}
