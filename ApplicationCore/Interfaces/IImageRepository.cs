﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IImageRepository
    {
        List<ProductImage> GetAlltoProduct(int productId);
    
    }
}
