﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetAllinCategory(int categoryId);
        List<Product> GetAllProductsFromCategoryAndChildrenCategories(int categoryId);
    }

}
