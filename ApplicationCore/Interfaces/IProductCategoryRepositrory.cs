﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IProductCategoryRepository
    {
        List<ProductCategory> GetProductsCategories(int productId);
    }
}
