﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Models
{
    public class ProductVM
    {
        public ProductVM()
        {
            Categories = new List<SelectListItem>();
            Variations = new List<ProductVarioation>();
            Images = new List<ProductImage>();
            Comments = new List<Comment>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public int[] SelectedCategories { get; set; }
        public ICollection<ProductVarioation> Variations { get; set; }
        public ICollection<ProductCategory> ProductCategories { get; set; }

        public ICollection<ProductImage> Images { get; set; }

        public ICollection<Comment> Comments { get; set; }


    }
}
