﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Models
{
    public class CategoryVM
    {
        public CategoryVM()
        {
            Categories = new List<SelectListItem>();
        }
        public int Id { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public int? ParentCategoryId { get; set; }
        public string Name { get; set; }
    }
}
