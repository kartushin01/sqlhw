﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace Web.Models
{
    public class ProductIndexVM
    {
        public IEnumerable<ProductVM> Products { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}
