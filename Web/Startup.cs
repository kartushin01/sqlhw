using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ADOLib.Repositories;
using ApplicationCore.Interfaces;
using EFLib;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Web.Interfaces;
using Web.Services;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<CatalogContext>(c =>
                c.UseSqlServer(Configuration.GetConnectionString("CatalogConnection")));

            services.AddScoped(typeof(IRepository<>), typeof(EfLib.Repositories.Repository<>));

            //ADO Repos
            //services.AddScoped<ICategoryRepository, CategoryRepository>();
            //services.AddScoped<IProductRepository, ProductRepository>();
            //services.AddScoped<IImageRepository, ImageRepository>();
            //services.AddScoped<IProductVariationReository, ProductVariationRepository>();
            //services.AddScoped<ICommentRepository, CommentsRepository>();
            //services.AddScoped<IProductCategoryRepository, ProductCategoryRepository>();
            //EF Repos


            services.AddScoped<ICategoryRepository, EfLib.Repositories.CategoryRepository>();
            services.AddScoped<IProductRepository, EfLib.Repositories.ProductRepository>();
            services.AddScoped<IImageRepository, EfLib.Repositories.ImageRepository>();
            services.AddScoped<IProductVariationReository, EfLib.Repositories.ProductVariationRepository>();
            services.AddScoped<ICommentRepository, EfLib.Repositories.CommentRepository>();
            services.AddScoped<IProductCategoryRepository, EfLib.Repositories.ProductCategoryRepository>();


            services.AddScoped<IProductViewModelService, ProductViewModelService>();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
