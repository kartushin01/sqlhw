﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EfLib.Repositories;
using Web.Interfaces;
using Web.Models;

namespace Web.Services
{
    public class ProductViewModelService : IProductViewModelService
    {

        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICommentRepository _commentReository;
        private readonly IImageRepository _imagesRepository;
        private readonly IProductVariationReository _variationReository;
        private readonly IProductCategoryRepository _productCategoryRepository;

        private readonly IRepository<Product> _productRepositoryT;
        private readonly IRepository<Category> _categoryRepositoryT;


        public ProductViewModelService(

            IProductCategoryRepository productCategoryRepository,

            IProductRepository productRepository,
            ICategoryRepository categoryRepository,
            ICommentRepository commentReository,
            IImageRepository imagesRepository,
            IProductVariationReository variationReository,
            IRepository<Product> productRepositoryT,
            IRepository<Category> categoryRepositoryT)
        {
            _productCategoryRepository = productCategoryRepository;
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _commentReository = commentReository;
            _imagesRepository = imagesRepository;
            _variationReository = variationReository;
            _productRepositoryT = productRepositoryT;
            _categoryRepositoryT = categoryRepositoryT;
        }

        public ProductVM GetProductVM(int id)
        {
            Product product = _productRepositoryT.GetById(id);
            ProductVM vm = new ProductVM();
            vm.Id = product.Id;
            vm.Name = product.Name;
            vm.Description = product.Description;
            vm.Variations = _variationReository.AllToProduct(product.Id);
            vm.Images = _imagesRepository.GetAlltoProduct(product.Id);
            vm.Comments = _commentReository.GetAllForProduct(product.Id);
            vm.ProductCategories =
                _productCategoryRepository.GetProductsCategories(product.Id);
            return vm;
        }

        public ProductIndexVM GetProductsListVM(int? categoryId)
        {
            List<Product> products = new List<Product>();

            Category category;

            if (categoryId != null)
            {
                category = _categoryRepositoryT.GetById((int)categoryId);

                products = _productRepository.GetAllProductsFromCategoryAndChildrenCategories(
                   (int)categoryId);
            }
            else
            {
                products = _productRepositoryT.GetAll() as List<Product>;
            }


            var vm = new ProductIndexVM()
            {
                Products = products.Select(i => new ProductVM()
                {
                    Id = i.Id,
                    Name = i.Name,
                    Description = i.Description,
                    Variations = _variationReository.AllToProduct(i.Id),
                    Images = _imagesRepository.GetAlltoProduct(i.Id),
                    Comments = _commentReository.GetAllForProduct(i.Id)
                }),
                Categories = _categoryRepository.GetAllWhitChildren()

            };

            return vm;
        }
    }
}
