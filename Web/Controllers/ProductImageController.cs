﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using EfLib.Repositories;

namespace Web.Controllers
{
    public class ProductImageController : Controller
    {
        private readonly IRepository<ProductImage> _imageRepository;
        public ProductImageController(IRepository<ProductImage> imageRepository)
        {
            _imageRepository = imageRepository;
        }


        // GET: ProductImage/Details/5
        public IActionResult Details(int id)
        {
            var img = _imageRepository.GetById(id);

            if (img == null)
            {
                return NotFound();
            }
            return View(img);
        }

        // GET: ProductImage/Create/productId
        public IActionResult Create(int productId)
        {

            return View();
        }

        // POST: ProductImage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Url,ProductId,Id")] ProductImage productImage)
        {
            if (ModelState.IsValid)
            {
                _imageRepository.Create(productImage);
                return RedirectToAction("Edit", "Product", new { id = productImage.ProductId });
            }
            return View(productImage);
        }

        // GET: ProductImage/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productImage = _imageRepository.GetById((int)id);
            if (productImage == null)
            {
                return NotFound();
            }

            return View(productImage);
        }

        // POST: ProductImage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Url,ProductId,Id")] ProductImage productImage)
        {
            if (id != productImage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _imageRepository.Update(productImage);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_imageRepository.IsOnlyOneEntity(productImage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction("Edit", "Product", new { id = productImage.ProductId });
            }

            return View(productImage);
        }

        // GET: ProductImage/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productImage = _imageRepository.GetById((int)id);

            if (productImage == null)
            {
                return NotFound();
            }

            return View(productImage);
        }

        // POST: ProductImage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {

            var img = _imageRepository.GetById(id);
            if (img != null)
            {
                _imageRepository.Delete(id);
            }

            return RedirectToAction("Edit", "Product", new { id = img.ProductId });
        }

    }
}
