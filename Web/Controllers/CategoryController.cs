﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using EfLib.Repositories;
using Web.Models;

namespace Web.Controllers
{
    public class CategoryController : Controller
    {
        
        private readonly IRepository<Category> _categoryRepository;
        public CategoryController(IRepository<Category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        // GET: Category
        public IActionResult Index()
        {
            var model = _categoryRepository.GetAll();
            return View(model);
        }

        // GET: Category/Details/5
        public  IActionResult Details(int id)
        {
            var category = _categoryRepository.GetById(id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // GET: Category/Create
        public IActionResult Create()
        {
            var model = new CategoryVM();

            model.Categories.Add(new SelectListItem() { Text = "", Value = "" });

            var categoriesListItems = _categoryRepository.GetAll().Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }).ToList();
            
            model.Categories.AddRange(categoriesListItems);

            return View(model);
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult Create([Bind("Name,Id, ParentCategoryId")] CategoryVM model)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category();
                category.Name = model.Name;
                if (model.ParentCategoryId != null)
                    category.ParentCategoryId = model.ParentCategoryId;

                _categoryRepository.Create(category);
               
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Category/Edit/5
        public  IActionResult Edit(int? id)
        {
            var category =  _categoryRepository.GetById((int) id);
            if (category == null)
            {
                return NotFound();
            }

            var model = new CategoryVM()
            {
                Name = category.Name,
                Id = category.Id
            };

            model.Categories.Add(new SelectListItem() { Text = "", Value = "" });

            var categories = _categoryRepository.GetAll();

            foreach (var item in categories)
            {
                bool selected = category.ParentCategoryId == item.Id;

                model.Categories.Add(new SelectListItem(){Text = item.Name, Value = item.Id.ToString(), Selected = selected});
            }

            return View(model);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult Edit(int id, [Bind("Name,Id, ParentCategoryId")] CategoryVM category)
        {
            if (id != category.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var categoryInDb = _categoryRepository.GetById(id);
                categoryInDb.Name = category.Name;
                categoryInDb.ParentCategoryId = category.ParentCategoryId;
                
                try
                {
                    _categoryRepository.Update(categoryInDb);
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_categoryRepository.IsOnlyOneEntity(category.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // GET: Category/Delete/5
        public  IActionResult Delete(int id)
        {
            var category = _categoryRepository.GetById(id);
            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        // POST: Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public  IActionResult DeleteConfirmed(int id)
        {
            var category =  _categoryRepository.GetById(id);
           _categoryRepository.Delete(category);
            return RedirectToAction(nameof(Index));
        }

      
    }
}
