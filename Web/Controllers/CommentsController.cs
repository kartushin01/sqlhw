﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using EfLib.Repositories;

namespace Web.Controllers
{
    public class CommentsController : Controller
    {

        private readonly IRepository<Comment> _commentRepository;
        private readonly IRepository<Product> _productRepository;
        public CommentsController(IRepository<Product> productRepository, IRepository<Comment> commentRepository)
        {
            _productRepository = productRepository;
            _commentRepository = commentRepository;
        }

        // GET: Comments
        public IActionResult Index()
        {
            var model = _commentRepository.GetAll();
            return View(model);
        }


        // GET: Comments/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var comment = _commentRepository.GetById((int)id);

            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // GET: Comments/Create/{int pdoructId}
        public IActionResult Create(int productId)
        {
            var product = _productRepository.GetById(productId);
            if (product == null)
            {
                return NotFound();
            }

            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Text,Rating,Author,DatePublish,ProductId,Id")] Comment comment)
        {
            if (ModelState.IsValid)
            {
                _commentRepository.Create(comment);
                return RedirectToAction("Edit", "Product", new { id = comment.ProductId });
            }

            return View(comment);
        }

        // GET: Comments/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRepository.GetById((int)id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Text,Rating,Author,DatePublish,ProductId,Id")] Comment comment)
        {
            if (id != comment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _commentRepository.Update(comment);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_commentRepository.IsOnlyOneEntity(comment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", "Product", new { id = comment.ProductId });
            }

            return View(comment);
        }

        // GET: Comments/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = _commentRepository.GetById((int)id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var comment = _commentRepository.GetById(id);
            if (comment != null)
            {
                _commentRepository.GetById(id);
                return RedirectToAction("Edit", "Product", new { id = comment.ProductId });
            }

            return View();
        }

    }
}
