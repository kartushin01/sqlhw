﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using EfLib.Repositories;
using Web.Interfaces;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductViewModelService _productService;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Category> _categoryRepository;

        public ProductController(IRepository<Product> productRepository, IProductViewModelService productService,
            IRepository<Category> categoryRepository)
        {
            _productRepository = productRepository;
            _productService = productService;
            _categoryRepository = categoryRepository;
        }


        // GET: Product
        public IActionResult Index(int? categoryId)
        {
            var vm = _productService.GetProductsListVM(categoryId);
            return View(vm);
        }

        // GET: Product/Details/5
        public IActionResult Details(int id)
        {
            var product = _productRepository.GetById(id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Product/Create
        public IActionResult Create()
        {
            ProductVM model = new ProductVM();
            var categories = _categoryRepository.GetAll();
            model.ProductCategories = new List<ProductCategory>();
            model.Categories = GetCategoriesSelectList(categories, model);

            return View(model);
        }


        // POST: Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(
            [Bind("Name, Description, Id, SelectedCategories")]
            ProductVM model)
        {
            if (ModelState.IsValid)
            {
                Product product = new Product()
                {
                    Name = model.Name,
                    Description = model.Description
                };

                product.ProductCategories = model.SelectedCategories.Select(i =>
                    new ProductCategory()
                    {
                        CategoryId = i,
                        ProductId = product.Id
                    }).ToList();

                _productRepository.Create(product);

                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        // GET: Product/Edit/5
        public IActionResult Edit(int id)
        {
            IReadOnlyList<Category> categories = _categoryRepository.GetAll();

            ProductVM model = _productService.GetProductVM(id);

            model.Categories = GetCategoriesSelectList(categories, model);

            return View(model);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id,
            [Bind("Name,Description,Id,SelectedCategories")]
            ProductVM model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Product productInDb = _productRepository.GetById(id);

                productInDb.Name = model.Name;
                productInDb.Id = model.Id;
                productInDb.Description = model.Description;
                productInDb.ProductCategories = model.SelectedCategories.Select(
                    i => new ProductCategory()
                    {
                        CategoryId = i,
                        ProductId = model.Id
                    }).ToList();

                try
                {
                    _productRepository.Update(productInDb);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        // GET: Product/Delete/5
        public IActionResult Delete(int id)
        {
            var product = _productRepository.GetById(id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var product = _productRepository.GetById(id);
            _productRepository.Delete(product);
            return RedirectToAction(nameof(Index));
        }

        private List<SelectListItem> GetCategoriesSelectList(
            IReadOnlyList<Category> categories,
            ProductVM model)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            foreach (var category in categories)
            {
                bool selceted = false;
                SelectListGroup optionGroup = null;

                if (category.Children != null && category.Children.Count > 0)
                {
                    optionGroup = new SelectListGroup()
                    {
                        Name = category.Name
                    };
                    foreach (var ch in category.Children)
                    {
                        selectList.Add(new SelectListItem()
                        {
                            Text = ch.Name,
                            Value = ch.Id.ToString(),
                            Group = optionGroup,
                            Selected = ChekSelected(ch.Id,
                                model.ProductCategories)
                        });
                    }
                }
                else
                {
                    selectList.Add(new SelectListItem()
                    {
                        Text = category.Name,
                        Value = category.Id.ToString(),
                        Group = optionGroup,
                        Selected = ChekSelected(category.Id,
                            model.ProductCategories)
                    });
                }
            }

            return selectList;
        }

        private bool ChekSelected(int cId,
            ICollection<ProductCategory> categories)
        {
            return categories.Any(c => c.CategoryId == cId);
        }
    }
}
