﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using EfLib.Repositories;

namespace Web.Controllers
{
    public class VarioationController : Controller
    {

        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductVarioation> _variationRepository;
        public VarioationController(IRepository<Product> productRepository, IRepository<ProductVarioation> variationRepository)
        {
            _productRepository = productRepository;
            _variationRepository = variationRepository;
        }


        // GET: Varioation/Details/5
        public IActionResult Details(int id)
        {

            var productVarioation = _variationRepository.GetById(id);
            if (productVarioation == null)
            {
                return NotFound();
            }

            return View(productVarioation);
        }

        // GET: Varioation/Create
        public IActionResult Create(int productId)
        {
            ViewData["ProductName"] = _productRepository.GetById(productId).Name;
            return View();
        }

        // POST: Varioation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Article,Price,ProductId,Description")] ProductVarioation productVarioation)
        {
            if (ModelState.IsValid)
            {
                _variationRepository.Create(productVarioation);

                return RedirectToAction("Edit", "Product", new { id = productVarioation.ProductId });
            }

            return View(productVarioation);
        }

        // GET: Varioation/Edit/5
        public IActionResult Edit(int id)
        {

            var productVarioation = _variationRepository.GetById(id);
            if (productVarioation == null)
            {
                return NotFound();
            }

            return View(productVarioation);
        }

        // POST: Varioation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Article,Price,ProductId,Description,Id")] ProductVarioation productVarioation)
        {
            if (id != productVarioation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _variationRepository.Update(productVarioation);

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_variationRepository.IsOnlyOneEntity(productVarioation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", "Product", new { id = productVarioation.ProductId });
            }

            return View(productVarioation);
        }

        // GET: Varioation/Delete/5
        public IActionResult Delete(int id)
        {
            var productVarioation = _variationRepository.GetById(id);
            if (productVarioation == null)
            {
                return NotFound();
            }

            return View(productVarioation);
        }

        // POST: Varioation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var productVarioation = _variationRepository.GetById(id);
            if (productVarioation != null)
            {
                _variationRepository.Delete(productVarioation);
            }

            return RedirectToAction("Edit", "Product", new { id = productVarioation.ProductId });
        }

    }
}
