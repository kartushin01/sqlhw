﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace Web.Views.Shared.Components.CategoryTreeView
{
    public class CategoryViewComponentModel
    {
        public CategoryViewComponentModel()
        {
            Categories = new List<Category>();
        }
        public bool IsFirstCall { get; set; }
        public ICollection<Category> Categories { get; set; }
    }
}
