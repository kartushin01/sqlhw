﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using EFLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;

namespace Web.Views.Shared.Components.CategoryTreeView
{
   
    public class CategoryTreeView : ViewComponent
    {
        private readonly ICategoryRepository _categoryRepository;
        public CategoryTreeView(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public IViewComponentResult Invoke(ICollection<Category> categories, bool isFirstCall)
        {
            if (isFirstCall)
            {
                categories = GetBlogCategoryTree();
            }

            var viewModle = new CategoryViewComponentModel { IsFirstCall = isFirstCall, Categories = categories };
           return View(viewModle);
        
        }

        public List<Category> GetBlogCategoryTree()
        {

            return _categoryRepository.GetAllWhitChildren();
                //.Include(i => i.Children)
                //.AsEnumerable()
                //.Where(x => x.ParentCategoryId == null)
                //.ToList();

        }

    }
}
