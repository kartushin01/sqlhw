﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using EFLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;
using Web.Views.Shared.Components.CategoryTreeView;

namespace Web.Views.Shared.Components.CatalogNaviagation
{
    public class CatalogNaviagation : ViewComponent
    {
        private readonly CatalogContext _db;
        public CatalogNaviagation(CatalogContext db)
        {
            _db = db;
        }
        public async Task<ViewViewComponentResult> InvokeAsync(ICollection<Category> categories, bool isFirstCall)
        {
            if (isFirstCall)
            {
                categories = GetBlogCategoryTree();
            }

            var viewModle = new CategoryViewComponentModel { IsFirstCall = isFirstCall, Categories = categories };
            return View(viewModle);
        }

        public List<Category> GetBlogCategoryTree()
        {
            return _db.Categories
                .Include(i => i.Children)
                .AsEnumerable()
                .Where(x => x.ParentCategoryId == null)
                .ToList();

        }
    }
}
