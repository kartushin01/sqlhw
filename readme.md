** Приложение каталог товаров **
Архитектура:
- ApplicationCore
	- Модели сущностей
	- Интерфейсы сущностей
- EFLib
	- контекст базы и реализация интерефейсов для entity framework core 
- ADOLib
	- Реализация интерфейсов для ADONET 
- Web
	- веб приложение aspnet core 3 


Для инициализации базы даных применить миррацию : 
*dotnet-ef database update -c catalogcontext -p ../EFLib/EFLib.csproj -s Web.csproj

** Таблицы: **
- Categories - категроии товаров. Иерархическая, имеет поле ParentCategoryId 
-  Catalog - для сущностей Product (товаров)
-  Variations - Вариации товаров связь с Product один ко многим
-  Comments - комментарии для товаров. Связь один ко многоим (у одного товара много комментариев)
- ProductCategories	 - Каждый товар может быть в разных категориях 
-  Images - фото товаров. Один ко многим

Запросы:
Стандартные getById(), ListAll(), AllinCategory(int categoryId) crud's и в таком духе. Из интересного:
- IProductRepository
	- AllProductsFromCategoryAndChildrenCategories - выборка товраов по категории включая товары с дочерних категорий
- ICategoryReository
	- All() - Выборка иерархии категорий 
